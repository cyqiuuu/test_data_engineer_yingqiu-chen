def exercise_one():
  for num in range(1,101):
    if (num % 3 == 0) & (num % 5 == 0):
      print("ThreeFive")
    elif num % 3 == 0:
      print("Three")
    elif num % 5 == 0:
      print("Five")
    else:
      print(num)

# arr is a list of numbers
def find_missing_nb(arr):
  xlist = arr
  xlist = [int(xlist[i]) for i in range(len(xlist))]
  xlist.sort()
  sum = 0
  i = 0
  # Calculate the sum from 1 to n
  while i <= max(xlist):
    sum = sum + i
    i = i + 1
  # Calculate the sum of the list
  sumlist = 0
  for i in range(0,len(xlist)):
    sumlist = sumlist + xlist[i]
  # The difference is equal to the missing number
  number = sum - sumlist
  if number != 0:
    print(number)
  # If the difference is 0, the missing number is n
  else:
    print(max(xlist)+1)

# arr is a list of numbers
def sort_positive_number(arr):
  xlist = arr
  xlist = [int(xlist[i]) for i in range(len(xlist))]
  a = 0
  for i in range(0,len(xlist)):
    if xlist[i] >= 0:
      for j in range(i,len(xlist)):
        if xlist[j] >= 0:
          if xlist[i] > xlist[j]:
            xlist[i], xlist[j] = xlist[j], xlist[i]
  print(xlist)
